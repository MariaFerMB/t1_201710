package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static int numberElements(IntegersBag bag){
		return model.numberElements(bag);
	}
	
	public static int numerosPares(IntegersBag bag){
		return model.numerosPares(bag);
	}
	
	public static int numerosImpares(IntegersBag bag){
		return model.numerosImpares(bag);
	}
	
}
