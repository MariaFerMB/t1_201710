package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){				
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
		}
		return max;
	}
	
	public int getMin(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){				
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
		}
		return max;
	}

	public int numerosPares(IntegersBag bag){
		int contador =0;
		int actual;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				actual =iter.next();
				if(actual%2==0.0){
					contador++;
				}
			}
		}
		return contador;
	}
	
	public int numerosImpares(IntegersBag bag){
		int contador =0;
		int actual;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				actual =iter.next();
				if(actual%2!=0.0){
					contador++;
				}
			}
		}
		return contador;
	}
	
	
	public int numberElements(IntegersBag bag){	
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator(); 
			while(iter.hasNext()){
				length++;
				iter.next();
			}		
		}
		return length;
	}
}
